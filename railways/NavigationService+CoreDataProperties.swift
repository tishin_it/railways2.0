//
//  NavigationService+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 19/03/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//
//

import Foundation
import CoreData


extension NavigationService {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NavigationService> {
        return NSFetchRequest<NavigationService>(entityName: "NavigationService")
    }

    @NSManaged public var color_bg: String?
    @NSManaged public var coordX: Float
    @NSManaged public var coordY: Float
    @NSManaged public var email: String?
    @NSManaged public var floor: String?
    @NSManaged public var ico_url: String?
    @NSManaged public var id_cat: String?
    @NSManaged public var img_url: String?
    @NSManaged public var mode_work_weekdays: String?
    @NSManaged public var mode_work_weekend: String?
    @NSManaged public var name: String?
    @NSManaged public var nav_id: String?
    @NSManaged public var nav_zoneId: String?
    @NSManaged public var site: String?
    @NSManaged public var tel: String?
    @NSManaged public var tel2: String?
    @NSManaged public var text: String?
    @NSManaged public var venID: String?
    @NSManaged public var photos: NSSet?
    @NSManaged public var railway: NSSet?

}

// MARK: Generated accessors for photos
extension NavigationService {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: ServicePhoto)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: ServicePhoto)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}

// MARK: Generated accessors for railway
extension NavigationService {

    @objc(addRailwayObject:)
    @NSManaged public func addToRailway(_ value: RailwayStation)

    @objc(removeRailwayObject:)
    @NSManaged public func removeFromRailway(_ value: RailwayStation)

    @objc(addRailway:)
    @NSManaged public func addToRailway(_ values: NSSet)

    @objc(removeRailway:)
    @NSManaged public func removeFromRailway(_ values: NSSet)

}
