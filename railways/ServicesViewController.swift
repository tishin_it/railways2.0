//
//  ServicesViewController.swift
//  railways
//
//  Created by Artem Peskishev on 01.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class ServicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var titleString : String?
    var station : RailwayStation?

    var servicesArray = [Service]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let titleString = titleString {
            title = titleString
        }        
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.updateData()
        
        if let stationID = station?.stationID {
            stationNameLabel.text = station?.name
            DataManager.shared.getServicesForStation(stationID, completion: {_,_ in
                self.updateData()
            })
        }
    }
    
    func updateData() {
        if let station = self.station {
            if let services = station.services {
                self.servicesArray = services.allObjects as! [Service]
                self.servicesArray = self.servicesArray.sorted(by: { Int($0.sort!)! < Int($1.sort!)! })
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.titleLabel.text = servicesArray[indexPath.row].name
        
        if let text = servicesArray[indexPath.row].text, text.isEmpty != true {
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let text = servicesArray[indexPath.row].text, text.isEmpty != true {
            performSegue(withIdentifier: "showService", sender: servicesArray[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showService" {
            let vc = segue.destination as! ServiceViewController
            let service = sender as! Service
            vc.station = station
            vc.textToSet = service.text
            vc.titleString = service.name
        }
    }
}
