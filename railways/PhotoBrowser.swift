//
//  PhotoBrowser.swift
//  railways
//
//  Created by Artem Peskishev on 16.01.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class PhotoBrowser: SKPhotoBrowser {

    func canRotate() -> Void {}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
    }

}
