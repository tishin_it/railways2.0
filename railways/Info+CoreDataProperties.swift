//
//  Info+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 28.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension Info {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Info> {
        return NSFetchRequest<Info>(entityName: "Info");
    }

    @NSManaged public var data: String?
    @NSManaged public var id_parent: String?
    @NSManaged public var infoID: String?
    @NSManaged public var name: String?
    @NSManaged public var sort: String?
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var railwayStation: RailwayStation?

}
