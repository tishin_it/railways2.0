//
//  Schedule+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 24.01.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension Schedule {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Schedule> {
        return NSFetchRequest<Schedule>(entityName: "Schedule");
    }

    @NSManaged public var endStation: String?
    @NSManaged public var isSubscribed: Bool
    @NSManaged public var startStation: String?
    @NSManaged public var time: NSDate?
    @NSManaged public var trackNumber: String?
    @NSManaged public var trainNumber: String?
    @NSManaged public var trainType: String?
    @NSManaged public var isDeparture: Bool
    @NSManaged public var railway: RailwayStation?

}
