//
//  RailwayStation+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 19/03/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//
//

import Foundation
import CoreData


extension RailwayStation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RailwayStation> {
        return NSFetchRequest<RailwayStation>(entityName: "RailwayStation")
    }

    @NSManaged public var address: String?
    @NSManaged public var city: String?
    @NSManaged public var distance: Double
    @NSManaged public var images: String?
    @NSManaged public var lat: Float
    @NSManaged public var lon: Float
    @NSManaged public var metro: String?
    @NSManaged public var name: String?
    @NSManaged public var onlineTablo: Bool
    @NSManaged public var stationCode: String?
    @NSManaged public var stationID: String?
    @NSManaged public var terminalCode: String?
    @NSManaged public var arrivalTrains: NSSet?
    @NSManaged public var availibilities: NSSet?
    @NSManaged public var availPhotos: NSSet?
    @NSManaged public var departureTrains: NSSet?
    @NSManaged public var emergencyPhones: NSSet?
    @NSManaged public var infoObjects: NSSet?
    @NSManaged public var navigationCategories: NSSet?
    @NSManaged public var navigationServices: NSSet?
    @NSManaged public var photos: NSSet?
    @NSManaged public var services: NSSet?
    @NSManaged public var ticket: QueueTicket?

}

// MARK: Generated accessors for arrivalTrains
extension RailwayStation {

    @objc(addArrivalTrainsObject:)
    @NSManaged public func addToArrivalTrains(_ value: Schedule)

    @objc(removeArrivalTrainsObject:)
    @NSManaged public func removeFromArrivalTrains(_ value: Schedule)

    @objc(addArrivalTrains:)
    @NSManaged public func addToArrivalTrains(_ values: NSSet)

    @objc(removeArrivalTrains:)
    @NSManaged public func removeFromArrivalTrains(_ values: NSSet)

}

// MARK: Generated accessors for availibilities
extension RailwayStation {

    @objc(addAvailibilitiesObject:)
    @NSManaged public func addToAvailibilities(_ value: Availiability)

    @objc(removeAvailibilitiesObject:)
    @NSManaged public func removeFromAvailibilities(_ value: Availiability)

    @objc(addAvailibilities:)
    @NSManaged public func addToAvailibilities(_ values: NSSet)

    @objc(removeAvailibilities:)
    @NSManaged public func removeFromAvailibilities(_ values: NSSet)

}

// MARK: Generated accessors for availPhotos
extension RailwayStation {

    @objc(addAvailPhotosObject:)
    @NSManaged public func addToAvailPhotos(_ value: AvailPhoto)

    @objc(removeAvailPhotosObject:)
    @NSManaged public func removeFromAvailPhotos(_ value: AvailPhoto)

    @objc(addAvailPhotos:)
    @NSManaged public func addToAvailPhotos(_ values: NSSet)

    @objc(removeAvailPhotos:)
    @NSManaged public func removeFromAvailPhotos(_ values: NSSet)

}

// MARK: Generated accessors for departureTrains
extension RailwayStation {

    @objc(addDepartureTrainsObject:)
    @NSManaged public func addToDepartureTrains(_ value: Schedule)

    @objc(removeDepartureTrainsObject:)
    @NSManaged public func removeFromDepartureTrains(_ value: Schedule)

    @objc(addDepartureTrains:)
    @NSManaged public func addToDepartureTrains(_ values: NSSet)

    @objc(removeDepartureTrains:)
    @NSManaged public func removeFromDepartureTrains(_ values: NSSet)

}

// MARK: Generated accessors for emergencyPhones
extension RailwayStation {

    @objc(addEmergencyPhonesObject:)
    @NSManaged public func addToEmergencyPhones(_ value: EmergencyPhone)

    @objc(removeEmergencyPhonesObject:)
    @NSManaged public func removeFromEmergencyPhones(_ value: EmergencyPhone)

    @objc(addEmergencyPhones:)
    @NSManaged public func addToEmergencyPhones(_ values: NSSet)

    @objc(removeEmergencyPhones:)
    @NSManaged public func removeFromEmergencyPhones(_ values: NSSet)

}

// MARK: Generated accessors for infoObjects
extension RailwayStation {

    @objc(addInfoObjectsObject:)
    @NSManaged public func addToInfoObjects(_ value: Info)

    @objc(removeInfoObjectsObject:)
    @NSManaged public func removeFromInfoObjects(_ value: Info)

    @objc(addInfoObjects:)
    @NSManaged public func addToInfoObjects(_ values: NSSet)

    @objc(removeInfoObjects:)
    @NSManaged public func removeFromInfoObjects(_ values: NSSet)

}

// MARK: Generated accessors for navigationCategories
extension RailwayStation {

    @objc(addNavigationCategoriesObject:)
    @NSManaged public func addToNavigationCategories(_ value: NavigationCategory)

    @objc(removeNavigationCategoriesObject:)
    @NSManaged public func removeFromNavigationCategories(_ value: NavigationCategory)

    @objc(addNavigationCategories:)
    @NSManaged public func addToNavigationCategories(_ values: NSSet)

    @objc(removeNavigationCategories:)
    @NSManaged public func removeFromNavigationCategories(_ values: NSSet)

}

// MARK: Generated accessors for navigationServices
extension RailwayStation {

    @objc(addNavigationServicesObject:)
    @NSManaged public func addToNavigationServices(_ value: NavigationService)

    @objc(removeNavigationServicesObject:)
    @NSManaged public func removeFromNavigationServices(_ value: NavigationService)

    @objc(addNavigationServices:)
    @NSManaged public func addToNavigationServices(_ values: NSSet)

    @objc(removeNavigationServices:)
    @NSManaged public func removeFromNavigationServices(_ values: NSSet)

}

// MARK: Generated accessors for photos
extension RailwayStation {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: Photo)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: Photo)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}

// MARK: Generated accessors for services
extension RailwayStation {

    @objc(addServicesObject:)
    @NSManaged public func addToServices(_ value: Service)

    @objc(removeServicesObject:)
    @NSManaged public func removeFromServices(_ value: Service)

    @objc(addServices:)
    @NSManaged public func addToServices(_ values: NSSet)

    @objc(removeServices:)
    @NSManaged public func removeFromServices(_ values: NSSet)

}
