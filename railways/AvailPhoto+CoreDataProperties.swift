//
//  AvailPhoto+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension AvailPhoto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AvailPhoto> {
        return NSFetchRequest<AvailPhoto>(entityName: "AvailPhoto");
    }

    @NSManaged public var url_foto_b: String?
    @NSManaged public var url_foto_m: String?
    @NSManaged public var railway: RailwayStation?

}
