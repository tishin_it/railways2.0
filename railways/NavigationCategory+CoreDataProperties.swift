//
//  NavigationCategory+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 19/03/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//
//

import Foundation
import CoreData


extension NavigationCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NavigationCategory> {
        return NSFetchRequest<NavigationCategory>(entityName: "NavigationCategory")
    }

    @NSManaged public var categoryID: String?
    @NSManaged public var ico_url: String?
    @NSManaged public var name: String?
    @NSManaged public var sort: String?
    @NSManaged public var railway: NSSet?

}

// MARK: Generated accessors for railway
extension NavigationCategory {

    @objc(addRailwayObject:)
    @NSManaged public func addToRailway(_ value: RailwayStation)

    @objc(removeRailwayObject:)
    @NSManaged public func removeFromRailway(_ value: RailwayStation)

    @objc(addRailway:)
    @NSManaged public func addToRailway(_ values: NSSet)

    @objc(removeRailway:)
    @NSManaged public func removeFromRailway(_ values: NSSet)

}
