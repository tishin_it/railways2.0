import Foundation

class PopupView: UIView {
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    var onConfirm: (()->())?
    var onDiscard: (()->())?

    
    func setup(text: String?) {
        confirmButton.setTitle("Подтвердить".localized(), for: .normal)
        cancelButton.setTitle("Отказаться".localized(), for: .normal)
        confirmButton.layer.cornerRadius = confirmButton.frame.size.height/2
        containerView.layer.cornerRadius = 15
        if let htmlString = text {
            
            let htmlWithFontString = String(format:"<html>\n <head>\n <style type=\"text/css\">\n body {font-family: \"%@\"; color:#354B59;}\n </style>\n </head>\n <body>%@</body>\n </html>","RobotoCondensed-Light",htmlString)
            
            webView.loadHTMLString(htmlWithFontString, baseURL: nil)
        }
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        onConfirm?()
    }
    
    @IBAction func discardButtonTapped(_ sender: UIButton) {
        onDiscard?()
    }
}
