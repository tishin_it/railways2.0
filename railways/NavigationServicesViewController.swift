//
//  NavigationServicesViewController.swift
//  railways
//
//  Created by Artem Peskishev on 20.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class NavigationServicesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var station : RailwayStation?
    var category : NavigationCategory?
    var venuesArray = [NavigationService]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationLabel: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let station = station {
            stationLabel.text = station.name
        }
        
        if let category = category {
            title = category.name
            if let services = station?.navigationServices, let servicesArray = Array(services) as? [NavigationService] {
                venuesArray = servicesArray.filter({ $0.id_cat == category.categoryID })
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venuesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCategoryCell") as! NavigationCategoryCell
        
        let venue = venuesArray[indexPath.row]
        cell.setCell(venue:venue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let venue = venuesArray[indexPath.row]
        performSegue(withIdentifier: "showServiceDetail", sender: venue)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showServiceDetail" {
            let vc = segue.destination as! NavigationServiceDetailViewController
            vc.venue = sender as! NavigationService?
            vc.station = station
        }
    }
}
