//
//  ScheduleCell.swift
//  railways
//
//  Created by Artem Peskishev on 01.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
protocol ScheduleCellDelegate : class {
    func didSelect(trainNumber : String)
    func removeAlarm()

    func didSubscribe(sched : Schedule)
    func didUnsubsribe(sched : Schedule)
}

class ScheduleCell: UITableViewCell {

    @IBOutlet weak var alarmButton: UIButton!
    @IBOutlet weak var dispatchLabel: UILabel!
    @IBOutlet weak var arrivalLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var trainLabel: UILabel!
    @IBOutlet weak var trainDescLabel: UILabel!
    @IBOutlet weak var roadLabel: UILabel!
    @IBOutlet weak var roadDescLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    var dateformatter = DateFormatter()

    weak var delegate : ScheduleCellDelegate?
    
    var schedule : Schedule?
    
    func setCell(schedule : Schedule) {
        self.schedule = schedule
        dispatchLabel.text = schedule.startStation
        arrivalLabel.text = schedule.endStation
        
        trainLabel.text = schedule.trainNumber
        trainDescLabel.text = "поезд".localized()
        roadDescLabel.text = "путь".localized()
        if schedule.trackNumber != "0" {
            if schedule.trackNumber != "" && schedule.trackNumber != nil {
                roadLabel.text = schedule.trackNumber
            } else {
                roadLabel.text = "-"
            }
            starButton.isHidden = true
        } else {
            starButton.isHidden = false
            roadLabel.text = "-"
        }
        
        let subscribedTrains = UserDefaults.standard.value(forKey: "subscribed") as! [String]
        if let index = subscribedTrains.index(of: schedule.trainNumber!) {
            starButton.isSelected = true
        } else {
            starButton.isSelected = false
        }

                
        dateformatter.dateFormat = "HH:mm"
        dateformatter.timeZone = NSTimeZone(name:"GMT") as TimeZone!
        let formattedString = dateformatter.string(from: schedule.time as! Date)
        timeLabel.text = formattedString
        alarmButton.isSelected = false
    }
    
    @IBAction func trainNotificationButtonTapped(_ sender: UIButton) {
        if alarmButton.isSelected != true {
            if let trainNumber = trainLabel.text {
                delegate?.didSelect(trainNumber: trainNumber)
            }
        } else {
            delegate?.removeAlarm()
        }
    }
    
    @IBAction func subscribeButtonTapped(_ sender: UIButton) {
        if let schedule = self.schedule {
            if starButton.isSelected != true {
                delegate?.didSubscribe(sched: schedule)
            } else {
                delegate?.didUnsubsribe(sched: schedule)
            }
        }
    }
}
