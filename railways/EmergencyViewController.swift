//
//  EmergencyViewController.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class EmergencyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var station : RailwayStation?
    var titleString : String?
    
    var phonesArray : [EmergencyPhone] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let titleString = titleString {
            title = titleString
        }
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let station = station {
            stationNameLabel.text = station.name
            if let emergencyPhones = station.emergencyPhones {
                phonesArray = emergencyPhones.allObjects as! [EmergencyPhone]
                phonesArray = phonesArray.sorted(by: { $0.name! < $1.name! })
                tableView.reloadData()
            }
        }
        
        if let stationID = station?.stationID {
            DataManager.shared.getEmergencyForStation(stationID, completion: {_,_ in
                if let station = self.station {
                    if let emergencyPhones = station.emergencyPhones {
                        self.phonesArray = emergencyPhones.allObjects as! [EmergencyPhone]
                        self.phonesArray = self.phonesArray.sorted(by: { $0.name! < $1.name! })
                        
                        var infoIndex = 0
                        
                        if self.phonesArray.count > 0 {
                            for i in (0...self.phonesArray.count-1) {
                                if self.phonesArray[i].name == "Единая справочная".localized() {
                                    infoIndex = i
                                    break
                                }
                            }
                            
                            let obj = self.phonesArray.remove(at: infoIndex)
                            self.phonesArray.insert(obj, at: 0)
                        }
                        
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phonesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneAndNavigationCell") as! PhoneAndNavigationCell
        
        let emergency = phonesArray[indexPath.row]
        
        cell.setCell(emergency:emergency)
        cell.delegate = self
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let vc = segue.destination as! NavigationViewController
            let predicate = NSPredicate(format: "name == %@ AND railwayStation.stationID == %@", "navigation", (station?.stationID)!)
            if let infoObject = Info.mr_findFirst(with: predicate) {
                if let jsonString = infoObject.data {
                    if let jsonDict = convertToDictionary(text: jsonString),
                        let mapNormal = jsonDict["map_normal"] as? String {
                        vc.locationName = mapNormal
                        if let mapInvalid = jsonDict["map_invalid"] as? String {
                            vc.locationInvName = mapInvalid
                        }
                    }
                }
            }
            
            vc.routeEmergency = sender as? EmergencyPhone
            vc.station = station
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension EmergencyViewController : PhoneAndNavigationCellDelegate {
    func navigateToEmergency(emergency: EmergencyPhone) {
        performSegue(withIdentifier: "showMap", sender: emergency)
    }
}
