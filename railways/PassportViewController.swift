//
//  PassportViewController.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class PassportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var titleString : String?
    var station : RailwayStation?
    var availArray = [Availiability]()
    var infoID : String?
    var photosArray = [AvailPhoto]()
    var images = [SKPhoto]()

    var type1rat : Float = 0
    var type2rat : Float = 0
    var type3rat : Float = 0
    var type4rat : Float = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var availiabilityLabel: UILabel!

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingView: UIView!
    
    @IBOutlet weak var type1Indicator: UIView!
    @IBOutlet weak var type2Indicator: UIView!
    @IBOutlet weak var type3Indicator: UIView!
    @IBOutlet weak var type4Indicator: UIView!

    @IBOutlet weak var tableButton: UIButton!
    @IBOutlet weak var collectionButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var mobilityView: UIView!
    @IBOutlet weak var mobilityLabel: UILabel!
    @IBOutlet weak var mobilityButtonHeight: NSLayoutConstraint!
    var mobilityInfoObject : Info?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableButton.setTitle("Доступность".localized(), for: .normal)
        availiabilityLabel.text = "Доступность элементов:".localized()
        collectionButton.setTitle("Фотографии".localized(), for: .normal)
        tableButton.backgroundColor = UIColor(red:0.76, green:0.80, blue:0.82, alpha:1.00)
        collectionButton.backgroundColor = UIColor(red:0.56, green:0.60, blue:0.62, alpha:1.00)
        
        ratingView.layer.cornerRadius = ratingView.frame.size.height/2
        type1Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type2Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type3Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type4Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        
        if let titleString = titleString {
            title = titleString
        }
        
        if let stationID = station?.stationID {
            titleLabel.text = station?.name
            updateData()
            DataManager.shared.getPassportForStation(stationID, completion: { _,_ in
                self.updateData()
            })
        }
        if let infoID = infoID {
            let predicate = NSPredicate(format: "id_parent = %@", infoID)
            if let childObjects = Info.mr_findAll(with: predicate), childObjects.count > 0 {
                if let mobiliyObject = childObjects[0] as? Info {
                    mobilityInfoObject = mobiliyObject
                    mobilityLabel.text = mobiliyObject.title
                }
            } else {
                mobilityButtonHeight.constant = 0
            }
        }

    }
    
    func updateData() {
        if let avails = station?.availibilities {
            availArray = avails.allObjects as! [Availiability]
            availArray = availArray.sorted(by: { Int($0.id_cat!)! < Int($1.id_cat!)! })
            tableView.reloadData()
            calcRating()
        }
        
        if let photos = station?.availPhotos {
            photosArray = photos.allObjects as! [AvailPhoto]
            photosArray = photosArray.sorted(by: { $0.url_foto_b! < $1.url_foto_b! })
            prepareSKPhotos()
            collectionView.reloadData()
        }
    }
    
    func prepareSKPhotos() {
        images = [SKPhoto]()
        for photo in photosArray {
            if let url = photo.url_foto_b {
                let skPhoto = SKPhoto.photoWithImageURL(url)
                images.append(skPhoto)
                skPhoto.shouldCachePhotoURLImage = false
            }
        }
    }
    
    func calcRating() {
        type1rat = 0
        type2rat = 0
        type3rat = 0
        type4rat = 0
        
        if availArray.count > 0 {
            for avail in availArray {
                if let type = avail.type1, let typeInt = Float(type) {
                    type1rat += typeInt
                }
                
                if let type = avail.type2, let typeInt = Float(type) {
                    type2rat += typeInt
                }
                
                if let type = avail.type3, let typeInt = Float(type) {
                    type3rat += typeInt
                }
                
                if let type = avail.type4, let typeInt = Float(type) {
                    type4rat += typeInt
                }
            }
            
            type1rat = type1rat/Float(availArray.count)
            type2rat = type2rat/Float(availArray.count)
            type3rat = type3rat/Float(availArray.count)
            type4rat = type4rat/Float(availArray.count)
            
            type1Indicator.backgroundColor = colorFrom(type: floor(type1rat))
            type2Indicator.backgroundColor = colorFrom(type: floor(type2rat))
            type3Indicator.backgroundColor = colorFrom(type: floor(type3rat))
            type4Indicator.backgroundColor = colorFrom(type: floor(type4rat))
            
            let percent = ((type1rat+type2rat+type3rat+type4rat)/8)*100
            
            ratingLabel.text = String(format:"%i%%", Int(percent.rounded()))
            
            ratingView.backgroundColor = colorFrom(type: floor(((type1rat+type2rat+type3rat+type4rat)/4)))

        }
    }
    
    func colorFrom(type:Float) -> UIColor {
        switch type {
        case 0:
            return UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
        case 1:
            return UIColor(red:0.96, green:0.51, blue:0.17, alpha:1.00)
        case 2:
            return UIColor(red:0.27, green:0.61, blue:0.15, alpha:1.00)
        default:
            return UIColor.white
        }
    }
    
    @IBAction func showTableView(_ sender: UIButton) {
        if collectionView.isHidden != true {
            mobilityView.isHidden = false
            collectionView.isHidden = true
            tableView.isHidden = false
            tableButton.backgroundColor = UIColor(red:0.76, green:0.80, blue:0.82, alpha:1.00)
            collectionButton.backgroundColor = UIColor(red:0.56, green:0.60, blue:0.62, alpha:1.00)
        } else {
            mobilityView.isHidden = true
            collectionView.isHidden = false
            tableView.isHidden = true
            tableButton.backgroundColor = UIColor(red:0.56, green:0.60, blue:0.62, alpha:1.00)
            collectionButton.backgroundColor = UIColor(red:0.76, green:0.80, blue:0.82, alpha:1.00)
        }
    }
    
    @IBAction func mobilityButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "showHTML", sender: mobilityInfoObject)
    }
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PassportCell") as! PassportCell
        
        cell.setCell(avail:availArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showAvailiability", sender: availArray[indexPath.row])
    }
    
    // MARK: CollectionView

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell",
                                                      for: indexPath) as! PhotoCell
        if let url = photosArray[indexPath.item].url_foto_m {
            cell.photoImageView.kf.setImage(with: URL(string:url))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let browser = PhotoBrowser(photos: images)
        browser.initializePageIndex(indexPath.item)
        present(browser, animated: true, completion: {})
        
    }

    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAvailiability" {
            let vc = segue.destination as! AvailiabilityViewController
            if let avail = sender as? Availiability {
                vc.avail = avail
            }
        } else if segue.identifier == "showHTML" {
            let infoObject = sender as! Info
            let vc = segue.destination as! WebViewViewController
            vc.station = station
            vc.htmlString = infoObject.data
            vc.titleString = infoObject.title
            if infoObject.title == "Контакты".localized() {
                vc.showButton = true
            }
        }
    }
    
}
