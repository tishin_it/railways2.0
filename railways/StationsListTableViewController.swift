//
//  StationsListTableViewController.swift
//  railways
//
//  Created by Artem Peskishev on 15.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import CoreData
import Localize_Swift

class StationsListTableViewController: UIViewController,NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate, LocationDelegate {

    var fetchedResultsController: NSFetchedResultsController<RailwayStation>?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationManager.sharedInstance.setupLocation()
        LocationManager.sharedInstance.delegate = self
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        title = "Навигация по вокзалам".localized()
        subtitleLabel.text = "Выберите вокзал".localized()
        searchField.placeholder = "Быстрый поиск".localized()
        
        updateData()
        
        DataManager.shared.getStationsList { (success, error) in
            self.updateData()
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(StationsListTableViewController.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        DataManager.shared.getStationsList { (success, error) in
            self.refreshControl.endRefreshing()
            self.updateData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let navController = navigationController {
            navController.navigationBar.shadowImage = UIImage()
        }
    }
    
    func updateData() {
        self.fetchedResultsController = RailwayStation.mr_fetchAllGrouped(by: nil, with: nil, sortedBy: "distance", ascending: true) as? NSFetchedResultsController<RailwayStation>
        if let searchText = searchField.text, searchText.characters.count > 0 {
        } else {
            self.tableView.reloadData()
        }
    }
 
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        if searchButton.isSelected == true {
            searchButton.isSelected = false
            searchField.text = ""
            updateData()
        }
    }
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections {
            let currentSection = sections[section]
            return currentSection.numberOfObjects
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationListCell") as! StationListCell
        
        let station = fetchedResultsController?.object(at: indexPath as IndexPath)
        
        cell.setStation(name: station?.name, city: station?.city, metro: station?.metro, distance: station?.distance)
        
        if let searchString = searchField.text, searchString.characters.count > 0 {

            
            if station?.name?.range(of: searchString, options: .caseInsensitive) != nil {
                cell.nameLabel.attributedText = colorString(searchString: searchString, originalString: cell.nameLabel.text!)
            }
            
            if station?.metro?.range(of: searchString, options: .caseInsensitive) != nil {
                cell.metroLabel.attributedText = colorString(searchString: searchString, originalString: cell.metroLabel.text!)
            }
            
            if station?.city?.range(of: searchString, options: .caseInsensitive) != nil {
                cell.metroLabel.attributedText = colorString(searchString: searchString, originalString: cell.metroLabel.text!)
            }
        }
        
        return cell
    }
    
    func colorString(searchString : String, originalString : String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: originalString)
        
        do {
            let regex = try NSRegularExpression(pattern: searchString, options: .caseInsensitive)
            let range = NSRange(location: 0, length: originalString.utf16.count)
            for match in regex.matches(in: originalString, options: .withTransparentBounds, range: range) {
                attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: match.range)
            }
        } catch _ {
            NSLog("Error creating regular expresion")
        }
        
        return attributedString
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        view.endEditing(true)
    }
    
    // MARK: TextField

    @IBAction func searchFieldEditingChanged(_ sender: UITextField) {
        if let searchString = sender.text, searchString.characters.count > 0 {
            let predicate = NSPredicate(format: "name contains[c] %@ OR city contains[c] %@ OR metro contains[c] %@ ", searchString, searchString, searchString)
            fetchedResultsController = RailwayStation.mr_fetchAllGrouped(by: nil, with: predicate, sortedBy: "distance", ascending: true) as? NSFetchedResultsController<RailwayStation>
            tableView.reloadData()
            searchButton.isSelected = true
        } else {
            searchButton.isSelected = false
            updateData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStation" {
            let stationInfoVC = segue.destination as! StationInfoViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                stationInfoVC.station = fetchedResultsController?.object(at: indexPath)
            }
        }
    }
    
    // MARK: LocationDelegate
    
    func didUpdateLocation() {
        
        if let stations = RailwayStation.mr_findAll() as? [RailwayStation] {
            for station in stations {
                station.distance = station.distanceBetween()
            }
            
            updateData()
        }

    }
}

