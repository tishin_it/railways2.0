//
//  CustomScrollView.swift
//  railways
//
//  Created by Artem Peskishev on 20.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class CustomScrollView: UIScrollView {

    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }
    
}
