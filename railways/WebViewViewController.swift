//
//  WebViewViewController.swift
//  railways
//
//  Created by Artem Peskishev on 29.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class WebViewViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet var showOnMap: UIButton!
    
    var htmlString : String?
    var station : RailwayStation?
    var titleString : String?
    
    var showButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.scrollView.showsHorizontalScrollIndicator = false
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.delegate = self
        showOnMap.setTitle("Показать на карте".localized(), for: .normal)
        
        if let htmlString = htmlString {
            
            var htmlWithFontString = String(format:"<html>\n <head>\n <style type=\"text/css\">\n body {font-family: \"%@\"; color:#354B59;}\n </style>\n </head>\n <body>%@</body>\n </html>","RobotoCondensed-Light",htmlString)
            
            if showButton == true {
                htmlWithFontString = htmlWithFontString.appending("<button type=\"button\" id=\"id\">Click Me!</button>")
            }
            
            webView.loadHTMLString(htmlWithFontString, baseURL: nil)
        }
        
        if let station = station {
            stationName.text = station.name
        }
        
        if let titleString = titleString {
            title = titleString
        }
    }
    
    @IBAction func showOnMapTapped(_ sender: UIButton) {
        let lat1 = self.station?.lat
        let lng1 = self.station?.lon
        
        let latitude:CLLocationDegrees =  Double(lat1!)
        let longitude:CLLocationDegrees =  Double(lng1!)
        
        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        if let stationName = self.station?.name {
            mapItem.name = stationName
        }
        mapItem.openInMaps(launchOptions: options)
    }
}

extension WebViewViewController : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if showButton == true {
            if let contentHeight = Float(webView.stringByEvaluatingJavaScript(from: "function f(){ var r = document.getElementById('id').getBoundingClientRect(); return r.top } f();")!) {
                webView.scrollView.addSubview(showOnMap)
                showOnMap.frame = CGRect(x:0,y:CGFloat(contentHeight),width:webView.frame.size.width, height: showOnMap.frame.size.height)
            }
        }
    }
}
