//
//  PhotoCell.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    
}
