//
//  QMSViewController.swift
//  railways
//
//  Created by Artem Peskishev on 17/02/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//

import UIKit
import MBProgressHUD
import MagicalRecord
import CoreLocation
import Localize_Swift

class QMSViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var station : RailwayStation?
    var titleString : String?
    var queueObject: Info?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet var checkView: QueueTicketView!
    @IBOutlet weak var avgServiceTimeLabel: UILabel!
    @IBOutlet weak var windowsCountLabel: UILabel!
    @IBOutlet weak var avgWaitTimeLabel: UILabel!
    @IBOutlet var popupView: PopupView!
    @IBOutlet weak var avgServiceTimeDescLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var min2Label: UILabel!
    @IBOutlet weak var avgWaitTimeDescLabel: UILabel!
    @IBOutlet weak var chooseServiceLabel: UILabel!
    @IBOutlet weak var operatorsCountLabel: UILabel!
    
    private var servicesList = [QueueServiceItem]()
    private var filteredServicesList = [QueueServiceItem]()
    private var ticket: QueueTicket?
    private var timer = Timer()

    private var gpsRange: Int?
    private var popupText: String?
    
    // MARK: - VIEW
    private var hud = MBProgressHUD()

    override func viewDidLoad() {
        super.viewDidLoad()
        avgServiceTimeDescLabel.text = "Среднее время обслуживания".localized()
        minLabel.text = "мин".localized()
        min2Label.text = "мин".localized()
        operatorsCountLabel.text = "Кол-во касс".localized()
        avgWaitTimeDescLabel.text = "Среднее время ожидания".localized()
        chooseServiceLabel.text = "Выберите услугу".localized()
        
        if let titleString = titleString {
            title = titleString
        }
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let station = station {
            stationNameLabel.text = station.name
        }
        
        view.addSubview(checkView)
        let checkHeight = tableView.frame.size.height > 400 ? tableView.frame.size.height : 400
        checkView.frame = CGRect(x:0, y:view.frame.size.height, width: 302, height: checkHeight + 80)
        checkView.center.x = view.center.x
        
        mapData()
        loadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer.invalidate()
    }
    
    private func mapData() {
        if let data = queueObject?.data,
            let json = convertToDictionary(text: data) {
            if let avgProcessTime = json["avg_process"] as? Int {
                avgServiceTimeLabel.text = "\(avgProcessTime/60/1000)"
            }
            
            if let avgWaitTime = json["avg_wait"] as? Int {
                avgWaitTimeLabel.text = "\(avgWaitTime/60/1000)"
            }
            
            if let windowCount = json["window_count"] as? Int {
                windowsCountLabel.text = "\(windowCount)"
            }
            
            popupText = json["popup_text"] as? String
            gpsRange = json["gps_range"] as? Int
        }
    }
    
    func loadData() {
        if let stationId = station?.stationID {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.label.text = "Загрузка".localized()
            
            DataManager.shared.getQueueFor(terminalId: stationId, completion: { (success, error, queueItemList) in
                self.checkTicket()
                if let itemList = queueItemList, success {
                    self.servicesList = itemList
                    self.filteredServicesList = self.servicesList.filter { $0.parentId == "0" }
                    self.tableView.reloadData()
                    self.hud.hide(animated: true)
                }  else if let error = error {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = error.localizedDescription
                    self.hud.hide(animated: true, afterDelay: 3)
                } else {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = "Повторите попытку позже".localized()
                    self.hud.hide(animated: true, afterDelay: 3)
                }
            })

        }
    }
    
    func checkTicket() {
        if let stationId = station?.stationID {
            let predicate = NSPredicate(format: "station.stationID == %@",stationId)

            if let ticket = QueueTicket.mr_findFirst(with: predicate) {
                self.ticket = ticket
                showTicket(ticket)
            }

        }
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredServicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.titleLabel.text = filteredServicesList[indexPath.row].title
        
        if let operationId = filteredServicesList[indexPath.row].operationId {
            let childArray = servicesList.filter({ $0.parentId == operationId})
            if childArray.count == 0 {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .disclosureIndicator
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let operationId = filteredServicesList[indexPath.row].operationId {
            let childArray = servicesList.filter({ $0.parentId == operationId})
            if childArray.count > 0 {
                performSegue(withIdentifier: "showDetailQMS", sender: childArray)
            } else {
                if let type = filteredServicesList[indexPath.row].type, type == "2" {
                    showPopup(onConfirm:{
                        self.popupView.removeFromSuperview()
                        self.registerTicket(operationId: operationId)
                    }, onCancel: {
                        self.popupView.removeFromSuperview()
                    })
                } else {
                    let message = "Вы уверены, что хотите встать в очередь для получения услуги ".localized()
                    let serviceName = filteredServicesList[indexPath.row].title ?? "" + "?"
                    let alert = UIAlertController(title: nil, message: message + serviceName, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ок".localized(), style: UIAlertActionStyle.default, handler: { _ in
                        self.registerTicket(operationId: operationId)

                    }))
                    alert.addAction(UIAlertAction(title: "Отмена".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailQMS" {
            let vc = segue.destination as! DetailQMSViewController
            vc.servicesList = servicesList
            vc.filteredList = sender as! [QueueServiceItem]
            vc.station = station
            vc.onComplete = { operationId in
                if let service = self.servicesList.first(where: { $0.operationId == operationId }),
                    let type = service.type, type == "2" {
                    self.showPopup(onConfirm:{
                        self.popupView.removeFromSuperview()
                        self.registerTicket(operationId: operationId)
                    }, onCancel: {
                        self.popupView.removeFromSuperview()
                    })
                } else {
                    self.registerTicket(operationId: operationId)
                }
            }
        }
    }
    
    // MARK: - CHECKVIEW
    
    func showTicket(_ ticket: QueueTicket) {
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(QMSViewController.updateTicketData(timer:)), userInfo: nil, repeats: true)
        
        self.ticket = ticket
        tableView.isUserInteractionEnabled = false
        checkView.setTicket(ticket)
        UIView.animate(withDuration: 0.2) {
            self.checkView.frame = CGRect(x:self.checkView.frame.origin.x, y: self.view.frame.size.height - self.checkView.frame.height + 30, width: self.checkView.frame.width, height: self.checkView.frame.height)
        }
    }
    
    func hideCheck() {
        tableView.isUserInteractionEnabled = true
        timer.invalidate()
        UIView.animate(withDuration: 0.2) {
            self.checkView.frame = CGRect(x:self.checkView.frame.origin.x, y: self.view.frame.size.height, width: self.checkView.frame.width, height: self.checkView.frame.height)
        }
    }
    
    @IBAction func checkViewButtonTapped(_ sender: UIButton) {
        if let ticket = ticket {
            if ticket.status == "0" || ticket.status == "9" {
                hideCheck()
                self.ticket?.mr_deleteEntity()
            } else {
                let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите выйти из очереди?".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Выйти".localized(), style: UIAlertActionStyle.default, handler: { _ in
                    self.unregisterTicket(ticket: ticket)
                }))
                alert.addAction(UIAlertAction(title: "Отмена".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    // MARK: - ticket action
    private func registerTicket(operationId: String) {
        guard checkDistance() else {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.text
            hud.label.text = "Вы должны находиться на вокзале".localized()
            hud.hide(animated: true, afterDelay: 3)
            return
        }
        
        if let stationId = station?.stationID {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.label.text = "Регистрация".localized()
            DataManager.shared.registerQueueTicketInfo(terminalId: stationId, operation_id: operationId, completion: { (success, error, ticket) in
                if let ticket = ticket {
                    self.showTicket(ticket)
                    self.hud.hide(animated: true)
                } else if let error = error {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = error.localizedDescription
                    self.hud.hide(animated: true, afterDelay: 3)
                } else {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = "Повторите попытку позже".localized()
                    self.hud.hide(animated: true, afterDelay: 3)
                }
            })
        }
    }
    
    private func unregisterTicket(ticket: QueueTicket) {
        if let stationId = station?.stationID,
            let ticketID = ticket.ticket_id {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.label.text = "Выходим из очереди".localized()
            DataManager.shared.cancelQueueTicketInfo(terminalId: stationId, ticket_id: ticketID, completion: { (success, error) in
                if success {
                    self.hud.hide(animated: true)
                    self.hideCheck()
                    self.timer.invalidate()
                } else if let error = error {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = error.localizedDescription
                    self.hud.hide(animated: true, afterDelay: 3)
                } else {
                    self.hud.mode = MBProgressHUDMode.text
                    self.hud.label.text = "Повторите попытку позже".localized()
                    self.hud.hide(animated: true, afterDelay: 3)
                }
            })

        }
    }
    
    @objc private func updateTicketData(timer: Timer) {
        if let stationId = station?.stationID,
            let ticketID = ticket?.ticket_id {
            DataManager.shared.getQueueTicketInfo(terminalId: stationId, ticketId: ticketID, completion: { (success, error, ticket) in
                if let ticket = ticket {
                    self.ticket = ticket
                    self.checkView.setTicket(ticket)
                }
            })
        }
    }
    
    private func showPopup(onConfirm:@escaping (()->()), onCancel:@escaping (()->())) {
        popupView.setup(text: popupText)
        popupView.frame = UIScreen.main.bounds
        popupView.onConfirm = onConfirm
        popupView.onDiscard = onCancel
        UIApplication.shared.keyWindow?.addSubview(popupView)
    }
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    private func checkDistance() -> Bool {
        return true
        if let currentLocation = LocationManager.sharedInstance.currentLocation(),
            let station = station,
            let gpsRange = gpsRange
        {
            let railwayLocation = CLLocation(latitude: CLLocationDegrees(station.lat), longitude: CLLocationDegrees(station.lon))
            return railwayLocation.distance(from: currentLocation) < Double(gpsRange)
        }
        return false
    }
}
