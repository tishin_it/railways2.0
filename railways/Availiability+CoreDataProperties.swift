//
//  Availiability+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension Availiability {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Availiability> {
        return NSFetchRequest<Availiability>(entityName: "Availiability");
    }

    @NSManaged public var id_cat: String?
    @NSManaged public var name_cat: String?
    @NSManaged public var description_cat: String?
    @NSManaged public var type1: String?
    @NSManaged public var type2: String?
    @NSManaged public var type3: String?
    @NSManaged public var type4: String?
    @NSManaged public var railway: RailwayStation?

}
