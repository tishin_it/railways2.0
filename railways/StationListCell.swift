//
//  StationListCell.swift
//  railways
//
//  Created by Artem Peskishev on 23.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class StationListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var metroLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    func setStation(name: String?,city: String?, metro: String?, distance: Double?) {
        
        if let name = name {
            nameLabel.text = name
        } else {
            nameLabel.text = "Not defined"
        }
        
        var addressString = ""
        
        if let city = city {
            addressString = city
        }
        
        if let metro = metro {
            if addressString.count > 0 {
                addressString.append(", ")
            }
            addressString.append(metro)
        }
        
        metroLabel.text = addressString
        
        if let distance = distance {
            if distance < 5 {
                let distanceStr = String(format:"~%.1f", distance)
                distanceLabel.text = distanceStr + "км".localized()
            } else {
                distanceLabel.text = "\(Int(distance))" + "км".localized()
            }
        } else {
            distanceLabel.text = ""
        }
    }
}
