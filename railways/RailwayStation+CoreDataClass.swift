//
//  RailwayStation+CoreDataClass.swift
//  railways
//
//  Created by Artem Peskishev on 23.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

@objc(RailwayStation)
public class RailwayStation: NSManagedObject {
    
    func distanceBetween() -> Double {

        if let currentLocation = LocationManager.sharedInstance.currentLocation() {
            let railwayLocation = CLLocation(latitude: CLLocationDegrees(self.lat), longitude: CLLocationDegrees(self.lon))
            return (railwayLocation.distance(from: currentLocation))/1000
        }
        return 0
    }
    
    override public func didImport(_ data: Any) {
        self.distance = distanceBetween()
    }
}
