//
//  DataManager.swift
//  railways
//
//  Created by Artem Peskishev on 15.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import MagicalRecord
import Localize_Swift

class DataManager: NSObject {

    static let shared = DataManager()
    let baseURL = "http://cmsapi.indigointeractive.ru/v2/"
    var langStr : String {
        get {
            return Localize.currentLanguage()
        }
    }

    func getStationsList(completion: @escaping (Bool, NSError?) -> Void) -> Void {

        Alamofire.request(baseURL, method: .get, parameters: ["method" : "term", "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String:AnyObject],
            let stationsJSON = JSON["response"] as? [[String:AnyObject]] {
                
                MagicalRecord.save({ (localContext) in
                
                    if let stationsArray = RailwayStation.mr_findAll() as? [RailwayStation] {
                        for station in stationsArray  {
                            if stationsJSON.first(where: { $0["id"] as? String == station.stationID }) == nil {
                                station.mr_deleteEntity(in: localContext)
                            }
                        }
                    }
                    
                    for jsonStation in stationsJSON {
                        if let stationID =  jsonStation["id"] {
                            let station = RailwayStation.mr_findFirstOrCreate(byAttribute: "stationID", withValue: stationID, in: localContext)
                            station.mr_importValuesForKeys(with: jsonStation)
                        }
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getInfoForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "info", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
                let responseJSON = JSON["response"] as? [String : AnyObject],
                var screensJSON = responseJSON["screens"] as? [[String : AnyObject]],
                let infoJSON = responseJSON["info"] as? [[String : AnyObject]] {
                
                MagicalRecord.save({ (localContext) in
                    var screensArray : [Info] = []
                    
                    if let infosArray = Info.mr_find(byAttribute: "railwayStation.stationID", withValue: stationID, in: localContext) as? [Info] {
                        for info in infosArray  {
                            if screensJSON.first(where: { $0["id"] as? String == info.infoID }) == nil {
                                info.mr_deleteEntity(in: localContext)
                            }
                        }
                    }
                    
                    for jsonScreen in screensJSON {
                        if let screenID =  jsonScreen["id"] {
                            let screen = Info.mr_findFirstOrCreate(byAttribute: "infoID", withValue: screenID, in: localContext)
                            screen.mr_importValuesForKeys(with: jsonScreen)
                            screensArray.append(screen)
                        }
                    }
                    
                    for jsonInfo in infoJSON {
                        if let infoID = jsonInfo["id"] {
                            let railwayStation = RailwayStation.mr_findFirstOrCreate(byAttribute: "stationID", withValue: infoID, in: localContext)
                            railwayStation.mr_importValuesForKeys(with: jsonInfo)
                            railwayStation.infoObjects = Set(screensArray) as NSSet?
                        }
                    }
                    
                }, completion: { (success, error) in
                    completion(true, nil)
                })
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getEmergencyForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "phone", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
            let responseJSON = JSON["response"] as? [[String : AnyObject]] {
                MagicalRecord.save({ (localContext) in
                    var phonesArray : [EmergencyPhone] = []
                    
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        for jsonPhone in responseJSON {
                            if let phone = EmergencyPhone.mr_createEntity(in: localContext) {
                                phone.mr_importValuesForKeys(with: jsonPhone)
                                phonesArray.append(phone)
                            }
                        }
                        station.emergencyPhones = Set(phonesArray) as NSSet?
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })

            } else {
                completion(false,nil)
            }
        }
    }
    
    func getPhotosForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "photo", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
                let responseJSON = JSON["response"] as? [[String : AnyObject]] {
                MagicalRecord.save({ (localContext) in
                    var photosArray : [Photo] = []
                    
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        for jsonPhoto in responseJSON {
                            if let photoID = jsonPhoto["id"] {
                                let photo = Photo.mr_findFirstOrCreate(byAttribute: "photoID", withValue: photoID, in: localContext)
                                photo.mr_importValuesForKeys(with: jsonPhoto)
                                photosArray.append(photo)
                            }
                        }
                        station.photos = Set(photosArray) as NSSet?
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
                
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getServicesForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "service", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
                let responseJSON = JSON["response"] as? [[String : AnyObject]] {
                MagicalRecord.save({ (localContext) in
                    var servicesArray : [Service] = []
                    
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        for jsonService in responseJSON {
                            if let service = Service.mr_createEntity(in: localContext) {
                                service.mr_importValuesForKeys(with: jsonService)
                                servicesArray.append(service)
                            }
                        }
                        station.services = Set(servicesArray) as NSSet?
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
                
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getNavigationServicesForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "service2", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : Any],
                let responseJSON = JSON["response"] as? [String : Any],
            let servicesJSON = responseJSON["service"] as? [[String: Any]],
            let categoriesJSON = responseJSON["cat"] as? [[String: Any]],
            let photosJSON = responseJSON["service_photo"] as? [[String: Any]] {
                MagicalRecord.save({ (localContext) in
                    var servicesArray : [NavigationService] = []
                    var categoriesArray : [NavigationCategory] = []
                    var photosArray : [ServicePhoto] = []
                    
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        if let navigationServices = station.navigationServices {
                            if let cachedServicesArray = Array(navigationServices) as? [NavigationService] {
                                for service in cachedServicesArray  {
                                    if servicesJSON.first(where: { $0["id"] as? String == service.venID }) == nil {
                                        service.mr_deleteEntity(in: localContext)
                                    }
                                }
                            }
                        }
                        
                        if let categories = station.navigationCategories {
                            if let cachedCategoriesArray = Array(categories) as? [NavigationCategory] {
                                for category in cachedCategoriesArray  {
                                    if categoriesJSON.first(where: { $0["id"] as? String == category.categoryID }) == nil {
                                        category.mr_deleteEntity(in: localContext)
                                    }
                                }
                            }
                        }
                        
                        ServicePhoto.mr_truncateAll(in: localContext)
                        
                        
                        for jsonService in servicesJSON {
                            if let serviceId = jsonService["id"] {
                                let service = NavigationService.mr_findFirstOrCreate(byAttribute: "venID", withValue: serviceId, in: localContext)
                                service.mr_importValuesForKeys(with: jsonService)
                                servicesArray.append(service)
                            }
                        }
                        
                        for jsonCategory in categoriesJSON {
                            if let categoryId = jsonCategory["id"] {
                                let category = NavigationCategory.mr_findFirstOrCreate(byAttribute: "categoryID", withValue: categoryId, in: localContext)
                                category.mr_importValuesForKeys(with: jsonCategory)
                                categoriesArray.append(category)
                            }
                        }
                        
                        for jsonPhoto in photosJSON {
                            if let photo = ServicePhoto.mr_createEntity(in: localContext) {
                                photo.mr_importValuesForKeys(with: jsonPhoto)
                                photosArray.append(photo)
                                
                                if let idService = photo.id_service, let service = NavigationService.mr_findFirst(byAttribute: "venID", withValue: idService, in: localContext) {
                                    service.addToPhotos(photo)
                                }
                            }
                        }
                        
                        station.navigationCategories = Set(categoriesArray) as NSSet?
                        station.navigationServices = Set(servicesArray) as NSSet?
                        
                        
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
                
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getScheduleForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "rasp", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
                let responseJSON = JSON["response"] as? [String : AnyObject] {
                MagicalRecord.save({ (localContext) in
                    
                    Schedule.mr_truncateAll(in: localContext)
                    
                    var departureTrains : [Schedule] = []
                    var arrivalTrains : [Schedule] = []
                    
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        if let arrivalJSON = responseJSON["arrival"] as? [[String : AnyObject]] {
                            for schedJSON in arrivalJSON {
                                if let schedule = Schedule.mr_createEntity(in: localContext) {
                                    schedule.mr_importValuesForKeys(with: schedJSON)
                                    schedule.isDeparture = false
                                    arrivalTrains.append(schedule)
                                }
                            }
                        }
                        
                        if let departureJSON = responseJSON["departure"] as? [[String : AnyObject]] {
                            for schedJSON in departureJSON {
                                if let schedule = Schedule.mr_createEntity(in: localContext) {
                                    schedule.mr_importValuesForKeys(with: schedJSON)
                                    schedule.isDeparture = true
                                    departureTrains.append(schedule)
                                }
                            }
                        }
                        
                        station.departureTrains = Set(departureTrains) as NSSet?
                        station.arrivalTrains = Set(arrivalTrains) as NSSet?
                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
                
            } else {
                completion(false,nil)
            }
        }
    }
    
    func subscribeFor(train:String, stationID : String, device_id : String, action : String?, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        
        var parameters = ["term_id" : stationID, "train_number" : train, "device_id" : device_id, "device" : "1"]
        if let action = action {
            parameters["action"] = action
        }
        
        Alamofire.request(baseURL+"?method=rasp", method: .post, parameters: parameters).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject] {
                if let response = JSON["response"] as? Bool,
                    Bool(response) != true,
                    let error = JSON["error"] as? String {
                    let userInfo: [NSObject : AnyObject] =
                        [ NSLocalizedDescriptionKey as NSObject :  error as AnyObject,
                        NSLocalizedFailureReasonErrorKey as NSObject : error as AnyObject ]
                    let err = NSError(domain: "ResponseErrorDomain", code: 401, userInfo: userInfo as! [String : Any])
                    completion(false, err)
                    
                } else {
                    completion(true, nil)
                }
            } else {
                completion(false, response.result.error as NSError?)
            }
        }
    }
    
    func getPassportForStation(_ stationID : String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "passport", "term_id" : stationID, "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
                let responseJSON = JSON["response"] as? [String : AnyObject] {
                MagicalRecord.save({ (localContext) in
                    var availiabilities : [Availiability] = []
                    var photosArray : [AvailPhoto] = []
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: stationID, in: localContext) {
                        if let availJSON = responseJSON["info"] as? [[String : AnyObject]],
                            let photosJSON = responseJSON["photo"] as? [[String : AnyObject]] {
                            
                            for JSON in availJSON {
                                if let avail = Availiability.mr_createEntity(in: localContext) {
                                    avail.mr_importValuesForKeys(with: JSON)
                                    availiabilities.append(avail)
                                }
                            }
                            
                            for jsonPhoto in photosJSON {
                                if let photo = AvailPhoto.mr_createEntity(in: localContext) {
                                    photo.mr_importValuesForKeys(with: jsonPhoto)
                                    photosArray.append(photo)
                                }
                            }
                            
                        }
                        station.availibilities = Set(availiabilities) as NSSet?
                        station.availPhotos = Set(photosArray) as NSSet?

                    }
                }, completion: { (success, error) in
                    completion(true, nil)
                })
                
            } else {
                completion(false,nil)
            }
        }
    }
    
    func getSupportCategories(completion: @escaping (Bool, NSError?, [[String:Any]]?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "support", "lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : AnyObject],
            let responseJSON = JSON["response"] as? [[String : Any]] {
                completion(true, nil, responseJSON)
            } else {
                completion(false, nil, nil)
            }
        }
    }

    func sendFeedback(stationID:String,cat_id:String,comment:String,photos:[UIImage], venue: NavigationService? ,completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(stationID.data(using: String.Encoding.utf8)!, withName: "term_id")
                multipartFormData.append(cat_id.data(using: String.Encoding.utf8)!, withName: "cat_id")
                multipartFormData.append(comment.data(using: String.Encoding.utf8)!, withName: "comment")
                
                var deviceData = "iOS: " + UIDevice.current.systemVersion + " Device: " + UIDevice().type.rawValue
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    deviceData = deviceData + " AppVersion: " + version
                }
                
                if let venue = venue, let mappointID = venue.venID {
                    multipartFormData.append(mappointID.data(using: String.Encoding.utf8)!, withName: "map_point_id")
                }
                
                multipartFormData.append(deviceData.data(using: String.Encoding.utf8)!, withName: "device_param")

                for image in photos {
                    if let imageData = UIImageJPEGRepresentation(image, 1) {
                        multipartFormData.append(imageData, withName: "file[]", fileName: self.randomString(length: 10), mimeType: "image/png")
                    }
                }
        },
            to: self.baseURL+"?method=support", 
            method:.post,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        completion(true, nil)
                    }

                case .failure( _):
                    completion(false,nil)
                }
        })
    }
    
    // MARK: - queue
    func getQueueFor(terminalId: String, completion: @escaping (Bool, NSError?, [QueueServiceItem]?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "queue", "term_id": terminalId,"lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : Any],
                let responseJSON = JSON["response"] as? [[String : Any]] {
                let queueServiceList = responseJSON.flatMap({ return QueueServiceItem(parentId: $0["parent_id"] as? String,
                                                                                      operationId: $0["operation_id"] as? String,
                                                                                      title: $0["title"] as? String,
                                                                                      type: $0["type"] as? String)
                })
                
                completion(true, nil, queueServiceList)
            } else {
                if let JSON = response.result.value as? [String : Any],
                    let error = JSON["error"] as? String {
                    let userInfo: [NSObject : AnyObject] =
                        [ NSLocalizedDescriptionKey as NSObject :  error as AnyObject,
                          NSLocalizedFailureReasonErrorKey as NSObject : error as AnyObject ]
                    let err = NSError(domain: "ResponseErrorDomain", code: 401, userInfo: userInfo as! [String : Any])
                    completion(false, err, nil)
                } else {
                    completion(false, nil, nil)
                }
            }
        }
    }
    
    func getQueueTicketInfo(terminalId: String, ticketId: String, completion: @escaping (Bool, NSError?, QueueTicket?) -> Void) -> Void {
        Alamofire.request(baseURL, method: .get, parameters: ["method" : "queue", "term_id": terminalId, "ticket_id": ticketId,"lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : Any],
                let response = JSON["response"] as? [String : AnyObject] {
                MagicalRecord.save({ (localContext) in
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: terminalId, in: localContext) {

                        let ticket = QueueTicket.mr_findFirstOrCreate(byAttribute: "ticket_id", withValue: ticketId, in: localContext)
                        ticket.mr_importValuesForKeys(with: response)
                        ticket.station = station
                    }
                }, completion: { (success, error) in
                    let ticket = QueueTicket.mr_findFirst(byAttribute: "ticket_id", withValue: ticketId)
                    completion(true, nil, ticket)
                })
            } else {
                completion(false, nil, nil)
            }
        }
    }
    
    func registerQueueTicketInfo(terminalId: String, operation_id: String, completion: @escaping (Bool, NSError?, QueueTicket?) -> Void) -> Void {
        let uniqueId = ModuleProxy.shared.deviceId.count > 0 ? ModuleProxy.shared.deviceId : UIDevice.current.identifierForVendor!.uuidString
        
        Alamofire.request(baseURL+"?method=queue", method: .post, parameters: ["term_id": terminalId, "mobile_platform":"ios", "mobile_id":uniqueId,  "operation_id": operation_id, "action": "ticket_register", "device_id": ModuleProxy.shared.deviceId,"lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : Any],
                let responseJSON = JSON["response"] as? [String : AnyObject],
                let success = responseJSON["result"] as? Bool,
                success == true,
                let ticketId = responseJSON["ticket_id"] as? String {
                MagicalRecord.save({ (localContext) in
                    if let station = RailwayStation.mr_findFirst(byAttribute: "stationID", withValue: terminalId, in: localContext) {
                        let ticket = QueueTicket.mr_findFirstOrCreate(byAttribute: "ticket_id", withValue: ticketId, in: localContext)
                        ticket.mr_importValuesForKeys(with: responseJSON)
                        ticket.station = station
                    }
                }, completion: { (success, error) in
                    let ticket = QueueTicket.mr_findFirst(byAttribute: "ticket_id", withValue: ticketId)
                    completion(true, nil, ticket)
                })
            } else {
                if let JSON = response.result.value as? [String : Any],
                    let error = JSON["error"] as? String {
                    let userInfo: [NSObject : AnyObject] =
                        [ NSLocalizedDescriptionKey as NSObject :  error as AnyObject,
                          NSLocalizedFailureReasonErrorKey as NSObject : error as AnyObject ]
                    let err = NSError(domain: "ResponseErrorDomain", code: 401, userInfo: userInfo as! [String : Any])
                    completion(false, err, nil)
                } else {
                    completion(false, nil, nil)
                }
            }
        }
    }
    
    func cancelQueueTicketInfo(terminalId: String, ticket_id: String, completion: @escaping (Bool, NSError?) -> Void) -> Void {
        Alamofire.request(baseURL+"?method=queue", method: .post, parameters: ["term_id": terminalId, "ticket_id": ticket_id, "action": "ticket_cancel","lang" : langStr]).responseJSON { response in
            if let JSON = response.result.value as? [String : Any],
                let responseJSON = JSON["response"] as? [String : AnyObject],
                let success = responseJSON["result"] as? Bool,
                success == true {
                MagicalRecord.save({ (localContext) in
                    let ticket = QueueTicket.mr_findFirst(byAttribute: "ticket_id", withValue: ticket_id, in: localContext)
                    ticket?.mr_deleteEntity(in: localContext)
                }, completion: { (success, error) in
                    completion(true, nil)
                })
            } else {
                if let JSON = response.result.value as? [String : Any],
                    let error = JSON["error"] as? String {
                    let userInfo: [NSObject : AnyObject] =
                        [ NSLocalizedDescriptionKey as NSObject :  error as AnyObject,
                          NSLocalizedFailureReasonErrorKey as NSObject : error as AnyObject ]
                    let err = NSError(domain: "ResponseErrorDomain", code: 401, userInfo: userInfo as! [String : Any])
                    completion(false, err)
                } else {
                    completion(false, nil)
                }
                
            }
        }
    }
    
    
    
    // MARK: - helper
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString+".png"
    }
    
}
