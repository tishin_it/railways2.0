//
//  MapPoint.swift
//  railways
//
//  Created by Artem Peskishev on 15.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import Navigine

class MapPoint: UIButton {

    var sublocation : NCSublocation?
    var originalFrame : CGRect = CGRect.zero
    var originalCenter : CGPoint = CGPoint.zero {
        didSet {
            self.center = originalCenter
        }
    }
    
    var isHide = true {
        didSet {
            isHidden = isHide
        }
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
        self.setImage(UIImage(resourceName: "elmMapPin"), for: .normal)
        contentMode = .scaleAspectFit
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        self.originalFrame = frame
    }
    
    func resizeWith(zoom: CGFloat) {
        
        self.center = CGPoint(x:originalCenter.x*zoom, y: originalCenter.y*zoom)
    }

}
