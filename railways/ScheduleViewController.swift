//
//  ScheduleViewController.swift
//  railways
//
//  Created by Artem Peskishev on 01.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import QuartzCore
import SevenSwitch
import MBProgressHUD
import DZNEmptyDataSet

class ScheduleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ScheduleCellDelegate {

    var titleString : String?
    var station : RailwayStation?
    
    var scheduleArray = [Schedule]()
    var scheduleDict = [[String:[Schedule]]]()

    var searchPredicate : NSPredicate?
    
    
    @IBOutlet weak var trainTypeView: UIView!
    var trainTypeSwitch = SevenSwitch()
    @IBOutlet weak var arrivalTypeView: UIView!
    var arrivalTypeSwitch = SevenSwitch()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationViewTitle: UILabel!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var intercityTrainLabel: UILabel!
    @IBOutlet weak var incityTrainLabel: UILabel!
    @IBOutlet weak var arriveLabel: UILabel!
    @IBOutlet weak var departureLabel: UILabel!



    var refreshControl: UIRefreshControl!
    var timer = Timer()
    var hud = MBProgressHUD()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let titleString = titleString {
            title = titleString
        }
        
        intercityTrainLabel.text = "Дальние поезда".localized()
        incityTrainLabel.text = "Пригородные".localized()
        departureLabel.text = "Отправление".localized()
        arriveLabel.text = "Прибытие".localized()
        searchField.placeholder = "Поиск поезда".localized()
        
        arrivalTypeSwitch.frame = CGRect(x:0,y:0,width:arrivalTypeView.frame.size.width,height:arrivalTypeView.frame.size.height)
        arrivalTypeSwitch.activeColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        arrivalTypeSwitch.inactiveColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        arrivalTypeSwitch.onTintColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        arrivalTypeSwitch.borderColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        arrivalTypeSwitch.shadowColor = UIColor.clear
        arrivalTypeSwitch.thumbTintColor = UIColor(red:0.91, green:0.05, blue:0.10, alpha:1.00)
        arrivalTypeSwitch.addTarget(self, action: #selector(ScheduleViewController.arrivalTypeSwitchChanged(sender:)), for: UIControlEvents.valueChanged)
        arrivalTypeSwitch.setOn(true, animated: false)
        arrivalTypeView.addSubview(arrivalTypeSwitch)
        
        trainTypeSwitch.frame = CGRect(x:0,y:0,width:arrivalTypeView.frame.size.width,height:arrivalTypeView.frame.size.height)
        trainTypeSwitch.activeColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        trainTypeSwitch.inactiveColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        trainTypeSwitch.onTintColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        trainTypeSwitch.borderColor = UIColor(red:0.86, green:0.87, blue:0.87, alpha:1.00)
        trainTypeSwitch.shadowColor = UIColor.clear
        trainTypeSwitch.thumbTintColor = UIColor(red:0.91, green:0.05, blue:0.10, alpha:1.00)
        trainTypeSwitch.addTarget(self, action:#selector(ScheduleViewController.trainTypeSwitchChanged(sender:)), for: UIControlEvents.valueChanged)
        trainTypeView.addSubview(trainTypeSwitch)
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        if UserDefaults.standard.value(forKey: "subscribed") == nil {
            UserDefaults.standard.set([String](), forKey: "subscribed")
        } else if let oldSubscribed = UserDefaults.standard.value(forKey: "subscribed") as? String {
            UserDefaults.standard.set([String](), forKey: "subscribed")
        }
        
        self.updateData()

        if let stationID = station?.stationID {
            titleLabel.text = station?.name
            DataManager.shared.getScheduleForStation(stationID, completion: {_,_ in
                self.updateData()
            })
        }
        
        if let schedTrain = UserDefaults.standard.value(forKey: (station?.stationID)!) as? String {
            if let sched = Schedule.mr_findFirst(byAttribute: "trainNumber", withValue: schedTrain) {
                showNotificationView(train: sched)
            }
        } else {
            notificationView.isHidden  = true
            self.tableViewBottomConstraint.constant = 0
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ScheduleViewController.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        let nib = UINib(nibName: "ScheduleSectionHeader", bundle: Bundle.stations_frameworkBundle())
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "ScheduleSectionHeader")

    }
    
    @objc func refresh(sender:AnyObject) {
        if let stationID = station?.stationID {
            titleLabel.text = station?.name
            DataManager.shared.getScheduleForStation(stationID, completion: {_,_ in
                self.refreshControl.endRefreshing()
                self.updateData()
            })
        }
    }
    
    @objc func arrivalTypeSwitchChanged(sender:SevenSwitch) {
        updateData()
    }
    
    @objc func trainTypeSwitchChanged(sender:SevenSwitch) {
        updateData()
    }
    
    func updateData() {
        if let station = self.station {
            var trains : [Schedule]?
            if arrivalTypeSwitch.isOn() != true {
                trains = station.departureTrains?.allObjects as? [Schedule]
            } else {
                trains = station.arrivalTrains?.allObjects as? [Schedule]
            }
            if let unwrappedTrains = trains {
                self.scheduleArray = unwrappedTrains
                self.scheduleArray = self.scheduleArray.sorted(by: { $0.time?.compare($1.time as! Date) == .orderedAscending })
                
                if trainTypeSwitch.isOn() != true {
                    let predicate = NSPredicate(format: "trainType = '2' OR trainType = '3' OR trainType = '4'")
                    self.scheduleArray = self.scheduleArray.filter { predicate.evaluate(with: $0) }
                } else {
                    let predicate = NSPredicate(format: "trainType = '0' OR trainType = '1'")
                    self.scheduleArray = self.scheduleArray.filter { predicate.evaluate(with: $0) }
                }
                
                if let searchPredicate = searchPredicate {
                    self.scheduleArray = self.scheduleArray.filter { searchPredicate.evaluate(with: $0) }
                }
                
                let scheduleDict = dateGroup(things: self.scheduleArray)

                let df = DateFormatter()
                df.timeStyle = DateFormatter.Style.none
                df.dateStyle = DateFormatter.Style.long
                df.timeZone = NSTimeZone(name:"GMT") as TimeZone!

                self.scheduleDict = scheduleDict
                    //First map to an array tuples: [(Date, [String:Int]]
                    .map{(df.date(from: $0.key)!, [$0.key:$0.value])}
                    
                    //Now sort by the dates, using `<` since dates are Comparable.
                    .sorted{$0.0 < $1.0}
                    
                    //And re-map to discard the Date objects
                    .map{$1}
                
                self.tableView.reloadData()
            }
        }
    }
    
    func dateGroup(things:[Schedule]) -> [String:[Schedule]] {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeZone = NSTimeZone(name:"GMT") as TimeZone!

        return things.reduce([:]) { (reduction, aThing) -> [String:[Schedule]] in

            let localDate = dateFormatter.string(from: aThing.time as! Date)
            
            var result = reduction
            if var thingsForDay = result[localDate] {
                thingsForDay.append(aThing)
                result[localDate] = thingsForDay
            }
            else {
                result[localDate] = [aThing]
            }
            
            return result
        }
    }
    
    @IBAction func dismissNotificationButtonTapped(_ sender: Any) {
        removeAlarm()
    }
    
    // MARK: ScheduleCellDelegate
    
    func removeAlarm() {
        UserDefaults.standard.set(nil, forKey: (station?.stationID)!)
        removeNotifications()

        notificationView.isHidden = true
        timer.invalidate()
        self.tableViewBottomConstraint.constant = 0
        tableView.reloadData()
    }
    
    func didSelect(trainNumber: String) {
        UserDefaults.standard.setValue(trainNumber, forKey: (station?.stationID)!)
        if let sched = Schedule.mr_findFirst(byAttribute: "trainNumber", withValue: trainNumber) {
            showNotificationView(train: sched)
        }
        tableView.reloadData()
    }
    
    func removeNotifications() {
        if let scheduledLocalNotifications = UIApplication.shared.scheduledLocalNotifications {
            for notification in scheduledLocalNotifications {
                if let userInfoStationID = notification.userInfo?["stationID"] as? String, userInfoStationID == station?.stationID {
                    UIApplication.shared.cancelLocalNotification(notification)
                }
            }
        }
    }
    
    func didSubscribe(sched: Schedule) {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.numberOfLines = 0
        DataManager.shared.subscribeFor(train: sched.trainNumber!, stationID: (station?.stationID)!, device_id: ModuleProxy.shared.deviceId, action: nil) { (success, error) in
            if success != false {
                var subscribedTrains = UserDefaults.standard.value(forKey: "subscribed") as! [String]
                subscribedTrains.append(sched.trainNumber!)
                UserDefaults.standard.setValue(subscribedTrains, forKey: "subscribed")
                self.hud.mode = .text
                self.hud.label.text = "Ждите сообщения\n о назначении пути".localized()
                self.hud.hide(animated: true, afterDelay: 4)
            } else {
                self.hud.mode = .text
                self.hud.label.text = error?.localizedDescription
                if error?.code == -1009 {
                    self.hud.label.text = "Требуется подключение к интернету".localized()
                }
                
                self.hud.hide(animated: true, afterDelay: 3)
            }
            self.tableView.reloadData()
        }
    }
    
    func didUnsubsribe(sched: Schedule) {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Отписываемся от уведомления".localized()
        
        DataManager.shared.subscribeFor(train: sched.trainNumber!, stationID: (station?.stationID)!, device_id: ModuleProxy.shared.deviceId, action: "del") { (success, error) in
            if success != false {
                var subscribedTrains = UserDefaults.standard.value(forKey: "subscribed") as! [String]
                if let index = subscribedTrains.index(of: sched.trainNumber!) {
                    subscribedTrains.remove(at: index)
                }
                UserDefaults.standard.setValue(subscribedTrains, forKey: "subscribed")
                self.hud.hide(animated: true)
            } else {
                self.hud.mode = .text
                self.hud.label.text = error?.localizedDescription
                self.hud.hide(animated: true, afterDelay: 3)
            }
            self.tableView.reloadData()

        }
    }
    
    func setTimer(train: Schedule) {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ScheduleViewController.updateTimeLabel(timer:)), userInfo: train, repeats: true)
    }
    
    @objc func updateTimeLabel(timer: Timer) {
        let train = timer.userInfo as! Schedule
        let date = train.time?.addingTimeInterval(-60*60*3)
        if let secondsBetween = date?.timeIntervalSince(Date.init()) {
            if secondsBetween > 0{
                let unwrappedSeconds = Int(secondsBetween)
                let hours = unwrappedSeconds/3600
                let minutes = (unwrappedSeconds - hours * 3600)/60
                
                var departureString = ""
                if train.isDeparture != false {
                    departureString = "отправляется".localized()
                } else {
                    departureString = "прибывает".localized()
                }
                
                var stringToSet = "Поезд".localized() + " №\(train.trainNumber!)" + " \(departureString) " + "через".localized() + " "
                if hours > 0 {
                    stringToSet = stringToSet.appending(String(format:"%i %@ ",hours,"ч.".localized()))
                }
                
                if minutes > 0 {
                    stringToSet = stringToSet.appending(String(format:"%i %@",minutes, "мин.".localized()))
                }
                
                if hours == 0 && minutes == 0 {
                    stringToSet = stringToSet.appending(String(format:"%i %@",unwrappedSeconds,"сек.".localized()))
                }
                
                if let track = train.trackNumber, track != "0",  train.isDeparture != false {
                    stringToSet = stringToSet.appending(" " + "с пути".localized() + " \(track)")
                } else if let track = train.trackNumber, track != "0", track.isEmpty != true, train.isDeparture != true {
                    stringToSet = stringToSet.appending(" " + "на путь".localized() + " \(track)")
                }
                
                notificationViewTitle.text = stringToSet
            }  else {
                notificationViewTitle.text = String(format:NSLocalizedString("Поезд  №%@ ушел",comment:"") ,train.trainNumber!)
                timer.invalidate()
            }
        }
    }
    
    func showNotificationView(train: Schedule) {
        notificationView.isHidden = false
        self.tableViewBottomConstraint.constant = notificationView.frame.size.height
        timer.invalidate()
        // FIX IT: Хардкод только для москвы
        let date = train.time?.addingTimeInterval(-60*60*3)
        if let secondsBetween = date?.timeIntervalSince(Date.init()) {
            if secondsBetween > 0{
                let unwrappedSeconds = Int(secondsBetween)
                let hours = unwrappedSeconds/3600
                let minutes = (unwrappedSeconds - hours * 3600)/60
                
                var departureString = ""
                if train.isDeparture != false {
                    departureString = "отправляется".localized()
                } else {
                    departureString = "прибывает".localized()
                }
                
                var stringToSet = "Поезд".localized() + " №\(train.trainNumber!)" + " \(departureString) " + "через".localized() + " "
                if hours > 0 {
                    stringToSet = stringToSet.appending(String(format:"%i %@ ",hours,"ч.".localized()))
                }
                
                if minutes > 0 {
                    stringToSet = stringToSet.appending(String(format:"%i %@",minutes, "мин.".localized()))
                }
                
                if let track = train.trackNumber, track != "0", track.isEmpty != true, train.isDeparture != false {
                    stringToSet = stringToSet.appending(" " + "с пути".localized() + " \(track)")
                } else if let track = train.trackNumber, track != "0", track.isEmpty != true, train.isDeparture != true {
                    stringToSet = stringToSet.appending(" " + "на путь".localized() + " \(track)")
                }
                
                setTimer(train: train)
                
                notificationViewTitle.text = stringToSet
                
                removeNotifications()

                if hours > 12 {
                    let notification = UILocalNotification()
                    if let stationID = station?.stationID {
                        notification.userInfo = ["stationID" : stationID]
                    }
                    notification.alertBody = stringToSet
                    notification.fireDate = date?.addingTimeInterval(60*60*12) as Date?
                    UIApplication.shared.scheduleLocalNotification(notification)
                }
                
                if hours > 3 {
                    let notification = UILocalNotification()
                    if let stationID = station?.stationID {
                        notification.userInfo = ["stationID" : stationID]
                    }
                    notification.alertBody = stringToSet
                    notification.fireDate = date?.addingTimeInterval(60*60*3) as Date?
                    UIApplication.shared.scheduleLocalNotification(notification)
                }
                
                if hours > 1 {
                    let notification = UILocalNotification()
                    if let stationID = station?.stationID {
                        notification.userInfo = ["stationID" : stationID]
                    }
                    notification.alertBody = stringToSet
                    notification.fireDate = date?.addingTimeInterval(60*60*1) as Date?
                    UIApplication.shared.scheduleLocalNotification(notification)
                }
                
            } else {
                notificationViewTitle.text = String(format:NSLocalizedString("Поезд  №%@ ушел",comment:"") ,train.trainNumber!)

            }
        }
    }
    
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ScheduleSectionHeader") as! ScheduleSectionHeader
        let dict = scheduleDict[section]
        headerCell.headerLabel.text = Array(dict.keys)[0]
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return scheduleDict.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array(scheduleDict[section].values)[0].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell") as! ScheduleCell
        
        let sched = Array(scheduleDict[indexPath.section].values)[0][indexPath.row]
        
        cell.setCell(schedule: sched)
        
        if let schedTrain = UserDefaults.standard.value(forKey: (station?.stationID)!) as? String {
            if schedTrain == sched.trainNumber {
                cell.alarmButton.isSelected = true
            }
        }
        
        cell.delegate = self        
        
        if let searchString = searchField.text, searchString.characters.count > 0 {
            
            if sched.trainNumber?.range(of: searchString, options: .caseInsensitive) != nil{
                cell.trainLabel.attributedText = colorString(searchString: searchString, originalString: sched.trainNumber!)
            }
            
            if sched.startStation?.range(of: searchString, options: .caseInsensitive) != nil{
                cell.dispatchLabel.attributedText = colorString(searchString: searchString, originalString: sched.startStation!)
            }
            
            if sched.endStation?.range(of: searchString, options: .caseInsensitive) != nil{
                cell.arrivalLabel.attributedText = colorString(searchString: searchString, originalString: sched.endStation!)
            }
        }
        
        return cell
    }
    
    func colorString(searchString : String, originalString : String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: originalString)
        
        do {
            let regex = try NSRegularExpression(pattern: searchString, options: .caseInsensitive)
            let range = NSRange(location: 0, length: originalString.utf16.count)
            for match in regex.matches(in: originalString, options: .withTransparentBounds, range: range) {
                attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: match.range)
            }
        } catch _ {
            NSLog("Error creating regular expresion")
        }
        
        return attributedString
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: TextField
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        if searchButton.isSelected == true {
            searchButton.isSelected = false
            searchPredicate = nil
            searchField.text = ""
            updateData()
        }
    }
    
    @IBAction func searchFieldEditingChanged(_ sender: UITextField) {
        if let searchString = sender.text, searchString.characters.count > 0 {
            let predicate = NSPredicate(format: "TrainNumber contains[c] %@ OR startStation contains[c] %@ OR endStation contains[c] %@", searchString,searchString,searchString)
            searchPredicate = predicate
            searchButton.isSelected = true
        } else {
            searchPredicate = nil
            searchButton.isSelected = false
        }
        updateData()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

extension ScheduleViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Нет данных".localized()
        let attributes = [NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Regular", size: 16), NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}

