//
//  LinkViewController.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class LinkViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var cancelButton: UIButton!
    var webURL : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cancelButton.setTitle("Отмена".localized(), for: .normal)
        
        if let webURL = webURL,
            let url = URL(string: webURL){
            let requestObj = URLRequest(url: url)
            webView.loadRequest(requestObj)
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: { () -> Void in
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        })
    }

    func canRotate() -> Void {}

}
