import Navigine

class NavigationManager {
    static let sharedInstance : NavigationManager = {
        let instance = NavigationManager()
        return instance
        }()
    
    var navigineManager : NavigineCore
    
    init() {
        navigineManager = NavigineCore.init(userHash: "2639-ADE4-7ED2-CB2F", server: "https://rzd-api.navigine.com")
    }
}
